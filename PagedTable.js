/**
 * Table pagination/sorting/searching.
 */
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD
        define([], factory);
    } else if (typeof exports === 'object') {
        // Node, CommonJS-like
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.PagedTable = factory();
    }
}(this, function () {
    "use strict";

    /**
     * Represents a single table.
     * @constructor
     * @param {mixed} table - Table to make interactive. Can be an element ID, jQuery instance, or HTML element.
     */
    var PagedTable = function (table) {
        if (typeof table === 'undefined') {
            return null;
        }

        if (typeof table === 'string') {
            table = document.getElementById(table);
        } else if (window.jQuery && table instanceof jQuery) {
            table = table.get(0);
        } else if (table instanceof HTMLCollection) {
            table = table[0];
        }
        this.table = table;

        return this.init();
    };

    /**
     * Initialize the table by parsing data attributes, loading settings from localStorage, and creating the DOM nodes.
     */
    PagedTable.prototype.init = function () {
        this.tableHeader = this.table.getElementsByTagName('tr')[0];
        this.body = this.table.getElementsByTagName('tbody')[0];

        // process any attributes from the table to build our options
        this.makeOptions();

        // set some values to handle whatever we loaded from options or storage
        this.storageKey = "PagedTable." + this.table.id + ".";
        this.sorting = this.storageGetObject('sorting', []);
        this.itemsPerPage = this.storageGet('itemsPerPage', this.options.itemsPerPage, 10) * 1;
        this.startItem = this.storageGet('startItem', 0) * 1;
        this.query = this.storageGet('query', '');

        // process the row data first. then we can remove the rows from the dom before our other changes
        this.makeRowData();

        // now that we've emptied the table, add the other dom elements we need
        this.makeElements();
        this.makeColumns();
        this.createListeners();

        if (this.itemsPerPageDropdown) {
            this.itemsPerPageDropdown.value = this.itemsPerPage;
        }
        if (this.queryInput) {
            this.queryInput.value = this.query;
            this.search();
        }

        // now lets figure out what rows to display and display them
        this.sort();
        this.size();
        this.page();
    };

    /**
     * Process any attributes from the form and build our options object.
     */
    PagedTable.prototype.makeOptions = function () {
        var attributes = this.table.attributes;
        var options = {};
        for (var i = 0; i < attributes.length; ++i) {
            if (attributes[i].name.indexOf('table-') === 0) {
                options[camelCase(attributes[i].name.replace('table-', ''))] = attributes[i].value;
            }
        }

        this.options = this.extend({
            sortHeaderClass: 'fa-sort',
            sortAscClass: 'fa-sort-asc',
            sortDescClass: 'fa-sort-desc',
            startRecordClass: 'table-start-record',
            endRecordClass: 'table-end-record',
            footerClass: 'table-footer',
            totalRecordsClass: 'table-total-records',
            pageDropdownClass: 'table-page-dropdown',
            itemsPerPageDropdownClass: 'table-items-per-page',
            firstPageClass: 'table-first-page',
            prevPageClass: 'table-prev-page',
            nextPageClass: 'table-next-page',
            lastPageClass: 'table-last-page',
            queryClass: 'table-query',
            areaClass: 'table-area',
            itemsPerPage: 10,
            noResultsText: 'There are no records to display.'
        }, options);
    };

    /**
     * HTML templates for making the extra elements we need.
     */
    PagedTable.prototype.headerHtml = '<div class="col-xs-12"><div class="form-inline"><div class="form-group col-sm-6 col-xs-12"><div class="input-group"><span class="icon fa input-group-addon fa-search"></span><input type="text" class="form-control input-sm {0}" /></div></div><div class="col-sm-6 col-xs-12 text-right"><div class="input-group"><span class="input-group-addon">Per Page</span><select class="form-control input-sm {1}"><option value="10" selected="selected">10</option><option value="20">20</option><option value="50">50</option><option value="100">100</option></select></div></div></div></div>';
    PagedTable.prototype.footerHtml = '<div class="col-xs-12 text-center"><div class="col-sm-4 col-xs-12 {0}"><i class="fa fa-fast-backward btn {1}" alt="First Page"></i><i class="fa fa-step-backward btn {2}" alt="Previous Page"></i><i class="fa fa-step-forward btn {3}" alt="Next Page"></i><i class="fa fa-fast-forward btn {4}" alt="Last Page"></i></div><div class="col-sm-4 form-inline hidden-xs {0}"><div class="input-group"><span class="input-group-addon">Page</span><select class="{5} form-control"></select></div></div><div class="col-sm-4 col-xs-12 pull-right">Showing <span class="{6}"></span>-<span class="{7}"></span> of <span class="{8}"></span></div></div>';
    PagedTable.prototype.bodyHtml = '<div class="col-xs-12 {0}"></div>';

    /**
     * Make all the dom elements needed for searching, sorting, and paging.
     */
    PagedTable.prototype.makeElements = function () {
        var nodes = document.createDocumentFragment();

        var headerHtml = this.headerHtml.format(this.options.queryClass, this.options.itemsPerPageDropdownClass);
        var headerRange = document.createRange();
        nodes.appendChild(headerRange.createContextualFragment(headerHtml));

        var bodyHtml = this.bodyHtml.format(this.options.areaClass);
        var bodyRange = document.createRange();
        nodes.appendChild(bodyRange.createContextualFragment(bodyHtml));

        var footerHtml = this.footerHtml.format(this.options.footerClass, this.options.firstPageClass, this.options.prevPageClass, this.options.nextPageClass, this.options.lastPageClass, this.options.pageDropdownClass,
            this.options.startRecordClass, this.options.endRecordClass, this.options.totalRecordsClass);
        var footerRange = document.createRange();
        nodes.appendChild(footerRange.createContextualFragment(footerHtml));

        this.container = document.createElement('div');
        this.container.appendChild(nodes);
        this.table.parentNode.replaceChild(this.container, this.table);

        this.area = this.container.querySelector('.' + this.options.areaClass);
        this.area.appendChild(this.table);

        this.itemsPerPageDropdown = this.container.querySelector('.' + this.options.itemsPerPageDropdownClass);
        this.pageDropdown = this.container.querySelector('.' + this.options.pageDropdownClass);
        this.queryInput = this.container.querySelector('.' + this.options.queryClass);
    };

    /**
     * Manually add the event listeners.
     */
    PagedTable.prototype.createListeners = function () {
        if (this.queryInput) {
            this.queryInput.addEventListener('input', this.changeSearch.bind(this));
        }
        if (this.itemsPerPageDropdown) {
            this.itemsPerPageDropdown.addEventListener('change', this.changeSize.bind(this));
        }

        // paging buttons
        var elem = this.container.getElementsByClassName(this.options.firstPageClass);
        if (elem.length) {
            elem[0].addEventListener('click', this.move.bind(this, -1, true));
        }
        elem = this.container.getElementsByClassName(this.options.prevPageClass);
        if (elem.length) {
            elem[0].addEventListener('click', this.move.bind(this, -1, false));
        }
        elem = this.container.getElementsByClassName(this.options.nextPageClass);
        if (elem.length) {
            elem[0].addEventListener('click', this.move.bind(this, 1, false));
        }
        elem = this.container.getElementsByClassName(this.options.lastPageClass);
        if (elem.length) {
            elem[0].addEventListener('click', this.move.bind(this, 1, true));
        }

        if (this.pageDropdown) {
            this.pageDropdown.addEventListener('change', this.changePage.bind(this));
        }

    };

    /**
     * Reformat the column headers the way we need them for sorting.
     */
    PagedTable.prototype.makeColumns = function () {
        var i = 0;
        var columnCount = this.columnCount;
        var cells = this.tableHeader.cells;

        for (i; i < columnCount; ++i) {
            var cell = cells[i];
            if (cell.hasAttribute('data-type')) {
                cell.innerHTML += '<i class="pull-right fa ' + this.options.sortHeaderClass + '"></i>';
                cell.addEventListener('click', this.changeSort.bind(this, i));
            } else {
                cell.classList.add('nosort');
            }
            if (!cell.id) {
                cell.id = 'column' + i;
            }
        }
    };

    /**
     * Process the table rows and make all the objects in memory we need later.
     */
    PagedTable.prototype.makeRowData = function () {
        this.columnCount = this.tableHeader.cells.length;
        var i = 0, j = 0;
        var dataTypes = new Array(this.columnCount);
        for (i = 0; i < this.columnCount; ++i) {
            dataTypes[i] = this.tableHeader.cells[i].getAttribute('data-type');
        }

        var rows = this.body.rows;
        this.totalRecords = this.rowCount = rows.length;
        var rowData = new Array(this.rowCount);

        for (i = 0; i < this.rowCount; ++i) {
            var row = rows[i];

            var rowCols = new Array(this.columnCount);
            for (j = 0; j < this.columnCount; ++j) {
                var datatype = dataTypes[j];

                if (datatype !== null) {
                    var v = row.cells[j].innerText;
                    var val = '';
                    var obj = { text: v, value: v };
                    if (datatype === 'int') {
                        val = v * 1;
                    } else if (datatype === 'currency') {
                        val = v.replace(/(\$|\,)/g, '') * 1;
                    } else if (datatype === 'date') {
                        val = Date.parse(v);
                    } else {
                        obj.text = val = v.toLowerCase();
                    }
                    obj.value = isNaN(val) ? v.toLowerCase() : val;

                    rowCols[j] = obj;
                }
            }
            rowData[i] = { searchMatch: 1, columns: rowCols, o: i, rowBody: row };
        }
        this.rowData = rowData;

        // now empty out the table
        var firstChild = null;
        while ((firstChild = this.body.firstChild) !== null) {
            this.body.removeChild(firstChild);
        }
    };

    /**
     * Change the sorting order for the table.
     * @param {Number} index - Index of column to sort on
     * @param {Object} e - Event that triggered the change
     */
    PagedTable.prototype.changeSort = function (index, e) {
        this.startItem = 0;
        var val = getKVP(this.sorting, 'index', index);

        if (e.shiftKey) {
            document.getSelection().removeAllRanges();
        } else {
            this.sorting = [];
        }

        if (val === null) {
            this.sorting.push({ index: index, dir: 1 });
        } else {
            if (e.shiftKey) {
                if (val.dir === 0) {
                    this.sorting.splice(val._i, 1);
                } else {
                    val.dir = 0;
                    this.sorting[val._i] = val;
                }
            } else {
                val.dir = val.dir > 0 ? 0 : 1;
                this.sorting.push(val);
            }
        }

        this.sort();
        this.page();
    };

    /**
     * Change the size (# of items) of the table.
     */
    PagedTable.prototype.changeSize = function () {
        if (this.itemsPerPageDropdown) {
            this.itemsPerPage = this.itemsPerPageDropdown.value * 1;
            this.startItem = 0;
            this.size();
            this.page();
        }
    };

    /**
     * Change the search query for the table.
     */
    PagedTable.prototype.changeSearch = function () {
        if (this.queryInput) {
            this.query = this.queryInput.value;
            this.startItem = 0;
            this.search();
            this.size();
            this.page();
        }
    };

    /**
     * Move to another page.
     * @param {Number} d - Direction to move
     * @param {Number} m - Move to start/end if true
     */
    PagedTable.prototype.move = function (d, m) {
        this.changePage(d === 1 ? (m ? this.pageTotal : this.startItem / this.itemsPerPage + 2) : (m ? 1 : this.startItem / this.itemsPerPage));
    };

    /**
     * Change the currently viewed page.
     * @param {Number|Object} e - Page # or input for page # value
     */
    PagedTable.prototype.changePage = function (e) {
        var page = (isNaN(e) ? e.target.value : e) * 1;
        if (!isNaN(page) && page <= this.pageTotal && page > 0) {
            this.startItem = (page - 1) * this.itemsPerPage;
            this.page();
        }
    };

    /**
     * Sort the data for the table.
     */
    PagedTable.prototype.sort = function () {
        var table = this.table, i = 0;

        // set all the header cells to the correct icon
        for (i = 0; i < this.columnCount; ++i) {
            var c = this.tableHeader.cells[i];
            if (c.hasAttribute('data-type')) {
                var s = getKVP(this.sorting, 'index', i);
                var iElement = c.getElementsByTagName('i');
                if (iElement.length) {
                    iElement = iElement[0];
                }
                if (iElement) {
                    if (s === null) {
                        iElement.classList.add(this.options.sortHeaderClass);
                        iElement.classList.remove(this.options.sortAscClass);
                        iElement.classList.remove(this.options.sortDescClass);
                    } else {
                        iElement.classList.remove(this.options.sortHeaderClass);
                        s.dir !== 0 ? iElement.classList.add(this.options.sortAscClass) : iElement.classList.remove(this.options.sortAscClass);
                        s.dir === 0 ? iElement.classList.add(this.options.sortDescClass) : iElement.classList.remove(this.options.sortDescClass);
                    }
                }
            }
        }

        if (this.sorting.length > 0) {
            this.rowData.sort(compare.bind(this.sorting));
        } else {
            this.rowData.sort(defaultCompare);
        }
        this.storageSetObject('sorting', this.sorting);
    };

    /**
     * Figure out which rows to display.
     */
    PagedTable.prototype.page = function () {
        this.storageSet('startItem', this.startItem);

        if (this.pageDropdown) {
            this.pageDropdown.value = this.startItem / this.itemsPerPage + 1;
        }

        var i = 0, x = 0;
        var len = this.startItem + this.itemsPerPage;
        var t = this.container.getElementsByClassName(this.options.totalRecordsClass);
        if (t.length) {
            t[0].textContent = this.totalRecords;
        }

        var startRecord = this.startItem + 1;
        var elem = this.container.getElementsByClassName(this.options.startRecordClass);
        if (elem.length) {
            elem[0].textContent = this.totalRecords === 0 ? 0 : startRecord;
        }

        elem = this.container.getElementsByClassName(this.options.endRecordClass);
        if (elem.length) {
            elem[0].textContent = Math.min(startRecord + this.itemsPerPage - 1, this.rowCount, this.totalRecords);
        }

        var bodyFrag = document.createDocumentFragment();
        var vCount = 0;
        for (i; i < this.rowCount; ++i) {
            var r = this.rowData[i].rowBody;
            if (this.rowData[i].searchMatch) {
                if (x >= this.startItem && x < len) {
                    bodyFrag.appendChild(r);
                    ++vCount;
                }
                ++x;
            }
        }

        // using fragment and removeChild seems to be fastest. not intuitive though
        var body = this.body;
        var firstChild = null;
        while ((firstChild = body.firstChild) !== null) {
            body.removeChild(firstChild);
        }

        if (vCount > 0) {
            body.appendChild(bodyFrag);
        } else {
            // add a row that says `no matches found`
            var td = document.createElement('td');
            td.colSpan = this.columnCount;
            td.innerText = this.options.noResultsText;
            var tr = document.createElement('tr');
            tr.appendChild(td);
            body.appendChild(tr);
        }
    };

    /**
     * Update the page number dropdown.
     */
    PagedTable.prototype.size = function () {
        this.storageSet('itemsPerPage', this.itemsPerPage);

        this.pageTotal = Math.ceil(this.totalRecords / this.itemsPerPage);
        var f = this.container.getElementsByClassName(this.options.footerClass), i;
        if (f.length) {
            for (i = 0; i < f.length; ++i) {
                this.pageTotal < 2 ? f[i].classList.add('hide') : f[i].classList.remove('hide');
            }
        }

        var currentPage = this.startItem / this.itemsPerPage;
        if (this.pageDropdown) {
            var currentCount = this.pageDropdown.childNodes.length;
            i = currentCount;
            var lastChild = null;
            while (i > this.pageTotal && (lastChild = this.pageDropdown.lastChild) !== null) {
                this.pageDropdown.removeChild(lastChild);
                --i;
            }

            i = currentCount;
            if (i < this.pageTotal) {
                var optionFrag = document.createDocumentFragment();
                while (i < this.pageTotal) {
                    var o = document.createElement('option');
                    o.value = i + 1;
                    o.textContent = i + 1;
                    optionFrag.appendChild(o);
                    ++i;
                }
                this.pageDropdown.appendChild(optionFrag);
            }

            if (this.pageDropdown.childNodes.length > currentPage) {
                this.pageDropdown.childNodes[currentPage].selected = true;
            }
        }
    };

    /**
     * Search the data for the table and mark rows that match.
     */
    PagedTable.prototype.search = function () {
        var i = 0, x = 0, n = 0, v = 0, rowCount = this.rowCount, columnCount = this.columnCount;
        var query = this.query;
        this.storageSet('query', query);
        query = query.toLowerCase();

        var rowData = this.rowData;
        for (; i < rowCount; ++i) {
            v = 0;
            if (query === '') {
                v = 1;
            } else {
                var columns = rowData[i].columns;
                for (x = 0; x < columnCount; ++x) {
                    var b = columns[x];
                    if (b !== undefined && b.text.indexOf(query) > -1) {
                        v = 1;
                        break;
                    }
                }
            }
            if (v) {
                ++n;
            }
            rowData[i].searchMatch = v;
        }
        this.totalRecords = n;
    };

    /**
     * Get the value from localStorage. Coalesce the value and any other params until a defined valued is found.
     * @param {String} Name of property to get from storage
     * @returns {String} Value from storage
     */
    PagedTable.prototype.storageGet = function () {
        var i = 0, length = arguments.length, argument, result;
        arguments[0] = localStorage[this.storageKey + arguments[0]];
        for (; i < length; ++i) {
            // Only deal with non-null/undefined values
            if ((argument = arguments[i]) != null) {
                result = argument;
                break;
            }
        }
        return result;
    };

    /**
     * Save a value to localStorage.
     * @param {String} key - Name of property to get from storage
     * @param {String} value - String to save
     */
    PagedTable.prototype.storageSet = function (key, value) {
        localStorage[this.storageKey + key] = value;
    };

    /**
     * Stringify and save an object to localStorage.
     * @param {String} key - Name of property to save to storage
     * @param {Object} value - Object to save
     */
    PagedTable.prototype.storageSetObject = function (key, value) {
        localStorage[this.storageKey + key] = JSON.stringify(value);
    };

    /**
     * Retrieve a value form localStorage and convert to object.
     * @param {String} Name of property to get from storage
     * @returns {Object} Value from storage
     */
    PagedTable.prototype.storageGetObject = function () {
        var i = 0, length = arguments.length, argument, result;
        arguments[0] = localStorage[this.storageKey + arguments[0]];
        if (arguments[0]) {
            arguments[0] = JSON.parse(arguments[0]);
        }
        for (; i < length; ++i) {
            // Only deal with non-null/undefined values
            if ((argument = arguments[i]) != null) {
                result = argument;
                break;
            }
        }
        return result;
    };

    /**
     * Merge objects.
     * @param {Object} Objects to merge together
     * @returns {Object} Merged object
     */
    PagedTable.prototype.extend = function () {
        var result = {}, key, i, l = arguments.length;
        for (i = 0; i < l; i++) {
            for (key in arguments[i]) {
                if (arguments[i].hasOwnProperty(key)) {
                    if (typeof arguments[i][key] === 'undefined' || arguments[i][key] === null) {
                        continue;
                    } else {
                        result[key] = arguments[i][key];
                    }
                }
            }
        }
        return result;
    };

    /**
     * Find an object in an array where the property named key is set to value.
     * @param {Array} arr - Array to search for a match
     * @param {String} key - Name of property to match object on
     * @param {Mixed} value - Value to match to property
     * @returns {Object|null} Matching object
     */
    function getKVP(arr, key, value) {
        var i = arr.length - 1;
        while (i > -1) {
            if (arr[i][key] === value) {
                arr[i]._i = i;
                return arr[i];
            }
            --i;
        }
        return null;
    }

    /**
     * Format a string replacing {0}, {1}, etc with arguments in order.
     * @param {String} Params to replace into string
     * @returns {String} Updated string
     */
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] !== 'undefined' ? args[number] : match;
        });
    };

    /**
     * Default sorting function for the table - resets to order when table was loaded. 
     * @param {Object} a - Object to compare
     * @param {Object} b - Object to compare to
     * @returns {Number} Sort result
     */
    function defaultCompare(a, b) {
        return a.o > b.o ? 1 : a.o < b.o ? -1 : 0;
    }

    /**
     * Sorting function for the table. 
     * @this Sorting order
     * @param {Object} a - Object to compare
     * @param {Object} b - Object to compare to
     * @returns {Number} Sort result
     */
    function compare(a, b) {
        var i = 0, len = this.length;
        for (; i < len; ++i) {
            if (!a.columns.hasOwnProperty(this[i].index)) {
                return 0;
            }
            if (a.columns[this[i].index].value < b.columns[this[i].index].value) {
                return this[i].dir > 0 ? -1 : 1;
            }
            if (a.columns[this[i].index].value > b.columns[this[i].index].value) {
                return this[i].dir > 0 ? 1 : -1;
            }
        }
        return 0;
    }

    /**
     * Convert a name with dashes to camel case. 
     * @param {String} str - String to format
     * @returns {String} Updated string
     */
    function camelCase(str) {
        var replaceFunc = function (all, letter) {
            return letter.toUpperCase();
        };
        return str.replace(/-([a-z])/ig, replaceFunc);
    }

    return PagedTable;
}));
