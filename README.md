# PagedTable

Vanilla javascript library for making an HTML table interactive, allowing searching, sorting, and paging. See index.html for an example and more details.